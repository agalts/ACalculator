package com.eagalts.calculator

import android.annotation.SuppressLint
import android.content.ClipData
import android.content.ClipboardManager
import android.graphics.Color
import android.os.Bundle
import android.view.GestureDetector
import android.view.MotionEvent
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.example.calculator.R
import kotlinx.android.synthetic.main.activity_main.*
import org.mariuszgromada.math.mxparser.Expression
import kotlin.math.abs

@Suppress("DEPRECATION")
class MainActivity : AppCompatActivity(), GestureDetector.OnGestureListener {

    // Declaring gesture detector, swipe threshold, and swipe velocity threshold
    private lateinit var gestureDetector: GestureDetector
    private val swipeThreshold = 100
    private val swipeVelocityThreshold = 100

    private var mode = 0 // Current calculator mode (integer between 0 and N)

    private val maxOnLn = 20 // Maximum number of symbols on inline string

    private var valStrMath = ""
    private var dotCount = 0
    private var numCount = 1

    private var notClosedBr = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val tva : TextView = findViewById(R.id.LineAnswer)
        tva.text = "0"

        // Initializing the gesture detector
        gestureDetector = GestureDetector(this)


        // Functions that add a pressed button (number / action) to the line
        num1.setOnClickListener {addVal('1')}
        num2.setOnClickListener {addVal('2')}
        num3.setOnClickListener {addVal('3')}
        num4.setOnClickListener {addVal('4')}
        num5.setOnClickListener {addVal('5')}
        num6.setOnClickListener {addVal('6')}
        num7.setOnClickListener {addVal('7')}
        num8.setOnClickListener {addVal('8')}
        num9.setOnClickListener {addVal('9')}
        num0.setOnClickListener {addVal('0')}

        com.setOnClickListener {addVal('.')}
        btnClean.setOnClickListener { clearScreen() }

        Result.setOnClickListener {result()}

        LineAnswer.setOnClickListener { copyToClipboard() }
        LineMath.setOnClickListener { oneSymbolBack() }

        btnOpenBracket.setOnClickListener { addVal('(') }
        btnCloseBracket.setOnClickListener { addVal(')') }

        actDiv.setOnClickListener {
            when (mode) {
                0 -> addVal('÷')
                1 -> addVal('%')
                2 -> addTrVal("sin(")
                3 -> addTrVal("pi")
            }
        }

        actMul.setOnClickListener {
            when (mode) {
                0 -> addVal('×')
                1 -> addVal('^')
                2 -> addTrVal("cos(")
                3 -> addVal('e')
            }
        }

        actSub.setOnClickListener {
            when (mode) {
                0 -> addVal('-')
                1 -> addTrVal("sqrt(")
                2 -> addTrVal("tan(")
                3 -> addTrVal("log10(")
            }
        }

        actAdd.setOnClickListener {
            when (mode) {
                0 -> addVal('+')
                1 -> addVal('!')
                2 -> addTrVal("log")
                3 -> addTrVal("ln(")
            }
        }
    }

    // Override this method to recognize touch event
    override fun onTouchEvent(event: MotionEvent): Boolean {
        return if (gestureDetector.onTouchEvent(event)) {
            true
        }
        else {
            super.onTouchEvent(event)
        }
    }

    // All the below methods are GestureDetector.OnGestureListener members
    // Except onFling, all must "return false" if Boolean return type
    // and "return" if no return type
    override fun onDown(e: MotionEvent?): Boolean {
        return false
    }
    override fun onShowPress(e: MotionEvent?) {
        return
    }
    override fun onSingleTapUp(e: MotionEvent?): Boolean {
        return false
    }
    override fun onScroll(e1: MotionEvent?, e2: MotionEvent?, distanceX: Float, distanceY: Float): Boolean {
        return false
    }
    override fun onLongPress(e: MotionEvent?) {
        return
    }
    override fun onFling(e1: MotionEvent, e2: MotionEvent, velocityX: Float, velocityY: Float): Boolean {
        try {
            val diffY = e2.y - e1.y
            val diffX = e2.x - e1.x
            if (abs(diffX) > abs(diffY)) {
                if (abs(diffX) > swipeThreshold && abs(velocityX) > swipeVelocityThreshold) {
                    if (diffX > 0) {
                        // Detected Left to Right swipe gesture
                        changeMode(-1)
                    }
                    else {
                        // Detected Right to Left swipe gesture
                        changeMode(1)
                    }
                }
            }
        }
        catch (exception: Exception) {
            exception.printStackTrace()
        }
        return true
    }

    @SuppressLint("SetTextI18n")
    fun changeMode(change: Int) {
        // if change == 1 => the following mode is set
        // if change == -1 => the previous mode is set

        if ((mode == 0 && change == -1) || (mode == 3 && change == 1)) return
        mode += change

        when (mode) {
            0 -> {
                actDiv.text = "÷"
                actMul.text = "×"
                actSub.text = "-"
                actAdd.text = "+"

                setMainBtnColor("#FFFFFF")
            }
            1 -> {
                actDiv.text = "%"
                actMul.text = "^"
                actSub.text = "√"
                actAdd.text = "!"

                setMainBtnColor("#3b3eff")
            }
            2 -> {
                actDiv.text = "sin"
                actMul.text = "cos"
                actSub.text = "tan"
                actAdd.text = "log"

                setMainBtnColor("#3b3eff")
            }
            3 -> {
                actDiv.text = "π"
                actMul.text = "e"
                actSub.text = "lg"
                actAdd.text = "ln"

                setMainBtnColor("#3b3eff")
            }
        }
    }

    private fun setMainBtnColor(color:String) {
        actDiv.setTextColor(Color.parseColor(color))
        actMul.setTextColor(Color.parseColor(color))
        actSub.setTextColor(Color.parseColor(color))
        actAdd.setTextColor(Color.parseColor(color))
    }

    // Function that completely clears the screen by pressing the C button

    private fun clearScreen(){
        val tva : TextView = findViewById(R.id.LineAnswer)
        tva.text="0"

        valStrMath = ""
        notClosedBr = 0
        numCount = 1
        dotCount = 0

        val tvm : TextView = findViewById(R.id.LineMath)
        tvm.text = valStrMath

        checkBracket()
    }


    // Function that removes the last entered character in the action string

    private fun oneSymbolBack() {
        if (valStrMath.isEmpty()) return

        when(valStrMath.last()) {
            '.' -> dotCount--
            ')' -> notClosedBr++
            '(' -> notClosedBr--

            '÷', '×', '/', '*', '+', '-' -> numCount--

            'n', 's', 'g' -> {
                numCount--
                valStrMath = valStrMath.dropLast(1)

                when (valStrMath.last()) {
                    'o', 'i', 'a' -> valStrMath = valStrMath.dropLast(1)
                }
            }

            't' -> {
                numCount--
                valStrMath = valStrMath.dropLast(3)
            }

            'i' -> {
                numCount--
                valStrMath = valStrMath.dropLast(1)
            }
        }

        valStrMath = valStrMath.dropLast(1)

        val tvm : TextView = findViewById(R.id.LineMath)
        tvm.text = valStrMath
        checkBracket()
    }


    // Function that copies the result to the clipboard by clicking on the result itself

    private fun copyToClipboard() {
        val tva : TextView = findViewById(R.id.LineAnswer)
        val clipboard: ClipboardManager =
            getSystemService(CLIPBOARD_SERVICE) as ClipboardManager
        val clip = ClipData.newPlainText("Скопировано", tva.text)
        clipboard.setPrimaryClip(clip)
    }


    // Function that adding a symbol to a string

    private fun addVal(mc : Char) {
        val strMathSize = valStrMath.length
        var previous = '0'

        if (strMathSize >= maxOnLn) return
        else if (notClosedBr < 0) return

        if (strMathSize != 0) previous = valStrMath.last()

        when (mc) {
            '.' -> {
                if (dotCount == numCount) return
                else dotCount++
            }

            '(' -> notClosedBr++
            ')' -> notClosedBr--

            '÷', '×', '/', '*', '+', '-' -> {
                if (previous == '/' || previous == '+' || previous == '-' || previous == '*' || previous == '×' || previous == '÷')
                    return
                else numCount++
            }
        }

        val lineMath : TextView = findViewById(R.id.LineMath)

        valStrMath += mc
        lineMath.text = valStrMath
        checkBracket()
    }

    private fun addTrVal(mc : String) {
        val strMathSize = valStrMath.length

        numCount++
        if (mc.last() == '(') notClosedBr++

        if (strMathSize >= maxOnLn) return

        val lineMath : TextView = findViewById(R.id.LineMath)

        valStrMath += mc
        lineMath.text = valStrMath
        checkBracket()
    }


    // Function that checks for unclosed parentheses in a string

    private fun checkBracket() {
        val closeButton = findViewById<Button>(R.id.btnCloseBracket)
        val openButton = findViewById<Button>(R.id.btnOpenBracket)

        openButton.setTextColor(Color.parseColor("#FFFFFF"))

        if (notClosedBr >= 1)
            closeButton.setTextColor(Color.parseColor("#3b3eff"))
        else if (notClosedBr == 0)
            closeButton.setTextColor(Color.parseColor("#FFFFFF"))
        else {
            openButton.setTextColor(Color.parseColor("#ff483b"))
            closeButton.setTextColor(Color.parseColor("#ff483b"))
        }
    }


    // Function that reads the result of the input string

    private fun result() {
        if (valStrMath.isEmpty()) return
        when (valStrMath.last()) {
            '÷', '×','/', '*', '+', '-' -> return
        }

        valStrMath = valStrMath.replace('÷', '/')
        valStrMath = valStrMath.replace('×', '*')

        val tva : TextView = findViewById(R.id.LineAnswer)
        try {
            val eval = Expression(valStrMath).calculate()
            if (eval.isNaN()) {
                tva.text = "Ошибка"
            }
            else {
                if (eval.toInt().toDouble() == eval) {
                    tva.text = eval.toInt().toString()
                }
                else
                    tva.text = eval.toString()
            }
        }
        catch (e: Exception) {
            tva.text = "Ошибка"
        }
        valStrMath = findViewById<TextView>(R.id.LineMath).text as String
    }
}