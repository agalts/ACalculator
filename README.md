
# ACalculator

A simple calculator for Android smartphones.


## Tech Stack

**Android Studio:** Dolphin 2021.3.1 Path 1

**Tech-stack:**
- **Kotlin** (1.7.20) - code
- **Android XML** (1.0) - UI

**Additional libs:**
- [**mXparser**](https://mathparser.org/ "**mXparser**") (4.4.2) - responsible for the calculation


## Installation

- Recommended Android version: 12.0 and above. 
- Minimum Android version: 7.0 and above

Link to download: [download latest version (1.2)](https://gitlab.com/agalts/ACalculator/-/raw/main/release/app-release-v1.2.apk?inline=false "download latest version (1.2)")

    
## Features

- Basic arithmetic operations (addition, subtraction, multiplication, division)
- Arithmetic operations (percentage, exponentiation, square root, factorial)
- Trigonometric operations (sine, cosine, tangent, logarithm (decimal, natural))
- Values ​​of π and e
## Screenshot

![App Screenshot](https://ltdfoto.ru/images/2023/01/01/Screenshot_20230101-172050_KALKULYTOR.png )


## License
The program is distributed with open source code: [MIT](https://choosealicense.com/licenses/mit/)

